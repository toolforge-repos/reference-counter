# reference-counter
Flask API designed specifically to get the number of references for a given revision in some Wikimedia project and language.

Find more context on why this API is being developed on [this phabricator task](https://phabricator.wikimedia.org/T352177).

This version is running in [Toolforge](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web/Python), under the [reference-counter tool](https://toolsadmin.wikimedia.org/tools/id/reference-counter).

This API has two main endpoints to retrieve number of references for a given revision, one is based on wikitext, and the other one is based on HTML. [Some analyses](https://public-paws.wmcloud.org/User:Isaac%20(WMF)/HTML-dumps/references-wikitext-vs-html.ipynb#Stats) lead to the conclusion that calculating references based on HTML is more accurate than calculating references based on wikitext.

Part of the code is based on the already existing APIs to get references, such as [quality-revid-features](https://github.com/wikimedia/research-api-endpoint-template/blob/quality-article/model/wsgi.py#L621) and [articlequality](https://github.com/wikimedia/articlequality/blob/master/articlequality/feature_lists/enwiki.py#L49-L51).

## Implementation
### How to use
There are 4 different endpoints to retrieve the number of references for a given revision id. API GET requests can be done with the following format.

#### Based on Wikitext

```
https://reference-counter.toolforge.org/api/v1/references/<project>/<language>/<revision_id>
```

where `project` is the project name (such as *wikipedia* or *wikibooks*), `language` is the language code (such as *en* or *es*), and `revision_id` is the revision id.

There is also a debug endpoint to retrieve not only the number of references but also the wikitext used to calculate them.
This debug endpoint is intended to make it easier to check if the reference count is accurate, specifically for debugging purposes.

```
https://reference-counter.toolforge.org/api/v1/debug/references/<project>/<language>/<revision_id>
```

#### Based on HTML

```
https://reference-counter.toolforge.org/api/v1/references/html/<project>/<language>/<revision_id>
```

where `project` is the project name (such as *wikipedia* or *wikibooks*), `language` is the language code (such as *en* or *es*), and `revision_id` is the revision id.

There is also a debug endpoint to retrieve not only the number of references but also the HTML used to calculate them.
This debug endpoint is intended to make it easier to check if the reference count is accurate, specifically for debugging purposes.

```
https://reference-counter.toolforge.org/api/v1/debug/references/html/<project>/<language>/<revision_id>
```

### Example
Retrieve the number of references based on HTML for the revision id 1189646754 in the en.wikipedia wiki.

```
https://reference-counter.toolforge.org/api/v1/references/html/wikipedia/en/1189646754
```

### Maintenance

The API is deployed in Toolforge under the [reference-counter tool](https://toolsadmin.wikimedia.org/tools/id/reference-counter).

Maintainers need to be added to the specific Toolforge tool. For that purpose, having both a Wikimedia account and a developer account is necessary. You can find instructions on setting up your accounts in the [Toolforge Quickstart guide](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Quickstart). To join the reference-counter tool, follow [these instructions](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Tool_Accounts#Joining). For more details on deploying Flask APIs in Toolforge, refer to [this Python Toolforge guide](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web/Python). 


Once you become a maintainer, you can log in through SSH and access the codebase. If you need to make any changes to the source code of the API, modify the `app.py` file and restart the service.

**Note:** Ensure consistency updates between this repo and the source code in Toolforge. `references.py` in this repo should be the same as `app.py` in Toolforge.

```
# login Toolforge
$ ssh your-username@login.toolforge.org

# switch to the tool account
$ become reference-counter

# modify the source code
$ nano www/python/src/app.py

# restart service
$ webservice --backend=kubernetes python3.11 status
$ webservice --backend=kubernetes python3.11 restart
```

If changes require any additional packages, install them in the existing Python environment. In order to do that, you should first update the `requirements.txt` locally by using `pip freeze` (see next section). Then update the `requirements.txt` manually in the toolforge server, and run:

```
$ toolforge webservice --backend=kubernetes python3.11 shell
$ webservice-python-bootstrap --fresh
```
This should install all the required packages.

## Run the app locally
Running the app locally is pretty simple. First of all, you have to clone this repository. After that, you need to create a python environment and install requirements.

```
$ cd reference-counter

# create environment
$ python3 -m venv .venv

# activate environment
$ . .venv/bin/activate

# install packages
$ pip install -r ./requirements.txt
```

Once you have dependencies installed, you just need to start the API service locally.

```
$ flask --app references run
```

If you apply any change that requires new packages, you should install them in the environment, and then update the `requirements.txt` file.

```
# activate environment
$ . .venv/bin/activate

# install packages
$ pip install the-package-you-want-to-install

# update requirements.txt
$ pip freeze > ./requirements.txt
```
Do not forget to bootstrap the webservice in the toolforge machine with these new requirements (see previous section).
## Decisions/Concerns

### General
- The Wikidata project isn't covered by this API, as it operates differently from other projects. There already exists a specific Ruby gem called [wikidata-diff-analyzer](https://github.com/WikiEducationFoundation/wikidata-diff-analyzer) recommended for extracting references.

### About counting references from wikitext
- The API counts references based on the wikitext for the revision. It includes the self-closing tag `<ref/>` and the container version (`<ref>` and `</ref>`). It also takes shortened footnote templates into account.
- Shortened footnote templates are wiki-specific, varying across languages and projects. While it is possible to build wiki-specific lists of templates, the API employs a basic English set of templates for all languages. Many of these templates may not exist in languages other than English, but this shouldn't create false positives, as editors won't use them there (it'd be highly unlikely that the same template exists for another language but with a totally different purpose). It's always possible to add local-language variants of these templates for new wikis.
- The reference count isn't flawless. Some references may be overlooked, such as when a shortened footnote template is absent from the list. Conversely, false positives may occur. For example, revision [1189646754](https://en.wikipedia.org/w/index.php?title=Wikipedia:Citing_sources&action=edit&oldid=1189646754) has a lot of false positives due to the tag being inserted as code, a scenario the API doesn't handle with precision.
