from flask import Flask
import json
import mwapi
import re
import requests
from werkzeug.exceptions import HTTPException
from mwparserfromhtml import Article

app = Flask(__name__)

USER_AGENT = "https://toolsadmin.wikimedia.org/tools/id/reference-counter"

PROJECTS = [
    "wikipedia",
    "wikibooks",
    #"wikidata", wikidata project has its own way of counting references
    "wikimedia",
    "wikinews",
    "wikiquote",
    "wikisource",
    "wikiversity",
    "wikivoyage",
    "wiktionary",
]

# These templates are specific to English language.
SFN_TEMPLATES = [
    r"Shortened footnote template", r"sfn",
    r"Sfnp",
    r"Sfnm",
    r"Sfnmp"
]

LANGUAGE_CODES = ['aa', 'ab', 'ace', 'ady', 'af', 'ak', 'als', 'am', 'an', 'ang', 'ar', 'arc', 'ary', 'arz', 'as', 'ast', 'atj', 'av', 'avk', 'awa', 'ay', 'az', 'azb', 'ba', 'ban', 'bar', 'bat-smg', 'bcl', 'be', 'be-x-old', 'bg', 'bh', 'bi', 'bjn', 'bm', 'bn', 'bo', 'bpy', 'br', 'bs', 'bug', 'bxr', 'ca', 'cbk-zam', 'cdo', 'ce', 'ceb', 'ch', 'cho', 'chr', 'chy', 'ckb', 'co', 'cr', 'crh', 'cs', 'csb', 'cu', 'cv', 'cy', 'da', 'de', 'din', 'diq', 'dsb', 'dty', 'dv', 'dz', 'ee', 'el', 'eml', 'en', 'eo', 'es', 'et', 'eu', 'ext', 'fa', 'ff', 'fi', 'fiu-vro', 'fj', 'fo', 'fr', 'frp', 'frr', 'fur', 'fy', 'ga', 'gag', 'gan', 'gcr', 'gd', 'gl', 'glk', 'gn', 'gom', 'gor', 'got', 'gu', 'gv', 'ha', 'hak', 'haw', 'he', 'hi', 'hif', 'ho', 'hr', 'hsb', 'ht', 'hu', 'hy', 'hyw', 'hz', 'ia', 'id', 'ie', 'ig', 'ii', 'ik', 'ilo', 'inh', 'io', 'is', 'it', 'iu', 'ja', 'jam', 'jbo', 'jv', 'ka', 'kaa', 'kab', 'kbd', 'kbp', 'kg', 'ki', 'kj', 'kk', 'kl', 'km', 'kn', 'ko', 'koi', 'kr', 'krc', 'ks', 'ksh', 'ku', 'kv', 'kw', 'ky', 'la', 'lad', 'lb', 'lbe', 'lez', 'lfn', 'lg', 'li', 'lij', 'lld', 'lmo', 'ln', 'lo', 'lrc', 'lt', 'ltg', 'lv', 'mai', 'map-bms', 'mdf', 'mg', 'mh', 'mhr', 'mi', 'min', 'mk', 'ml', 'mn', 'mnw', 'mr', 'mrj', 'ms', 'mt', 'mus', 'mwl', 'my', 'myv', 'mzn', 'na', 'nah', 'nap', 'nds', 'nds-nl', 'ne', 'new', 'ng', 'nl', 'nn', 'no', 'nov', 'nqo', 'nrm', 'nso', 'nv', 'ny', 'oc', 'olo', 'om', 'or', 'os', 'pa', 'pag', 'pam', 'pap', 'pcd', 'pdc', 'pfl', 'pi', 'pih', 'pl', 'pms', 'pnb', 'pnt', 'ps', 'pt', 'qu', 'rm', 'rmy', 'rn', 'ro', 'roa-rup', 'roa-tara', 'ru', 'rue', 'rw', 'sa', 'sah', 'sat', 'sc', 'scn', 'sco', 'sd', 'se', 'sg', 'sh', 'shn', 'si', 'simple', 'sk', 'sl', 'sm', 'smn', 'sn', 'so', 'sq', 'sr', 'srn', 'ss', 'st', 'stq', 'su', 'sv', 'sw', 'szl', 'szy', 'ta', 'tcy', 'te', 'tet', 'tg', 'th', 'ti', 'tk', 'tl', 'tn', 'to', 'tpi', 'tr', 'ts', 'tt', 'tum', 'tw', 'ty', 'tyv', 'udm', 'ug', 'uk', 'ur', 'uz', 've', 'vec', 'vep', 'vi', 'vls', 'vo', 'wa', 'war', 'wo', 'wuu', 'xal', 'xh', 'xmf', 'yi', 'yo', 'za', 'zea', 'zh', 'zh-classical', 'zh-min-nan', 'zh-yue', 'zu']

# Add new code numbers as you need
CODE_NUMBERS = {'ratelimited': 429, 'nosuchrevid': 404, 'permissiondenied': 403}

@app.route("/")
def hello_world():
    return "<p>Hello, World! This is the References API.</p>"

@app.route('/api/v1/references/<project>/<lang>/<int:revid>')
def get_references_from_wikitext(project, lang, revid):
    error = validate_api_args(project, lang)
    if error:
        return {"description": error}, 400

    # TODO: get the wikitext by querying the replica db directly
    try:
        # Request the wikitext
        session = mwapi.Session(f'https://{lang}.{project}.org', user_agent=USER_AGENT)

        result = session.get(
                    action="parse",
                    oldid=revid,
                    prop='wikitext',
                    format='json',
                    formatversion=2
                )

    except mwapi.errors.APIError as e:
        return {"description": f"mwapi error: {e.code} - {e.info}"}, error_number(e.code)

    except mwapi.errors.ConnectionError as e:
        return {"description": f"mwapi error: Conection error - {e}"}, 502

    except Exception as e:
        return {"description": f"mwapi error: {e}, {type(e)}"}, 500

    try:
        wikitext = result['parse']['wikitext']

        num_ref_singleton, num_ref_tag, num_ref_sft = count_references(wikitext)
        num_ref = num_ref_singleton + num_ref_tag + num_ref_sft

        return {
            "project": project,
            "lang": lang,
            "revid": revid,
            "num_ref": num_ref,
            }
    except Exception as e:
        return {"description": f"Error calculating references from wikitext: {e}, {type(e)}"}, 500

@app.route('/api/v1/debug/references/<project>/<lang>/<int:revid>')
def get_references_debug_from_wikitext(project, lang, revid):
    error = validate_api_args(project, lang)
    if error:
        return {"description": error}, 400

    # TODO: get the wikitext by querying the replica db directly
    try:
        # Request the wikitext
        session = mwapi.Session(f'https://{lang}.{project}.org', user_agent=USER_AGENT)

        result = session.get(
                    action="parse",
                    oldid=revid,
                    prop='wikitext',
                    format='json',
                    formatversion=2
                )

    except mwapi.errors.APIError as e:
        return {"description": f"mwapi error: {e.code} - {e.info}"}, error_number(e.code)

    except mwapi.errors.ConnectionError as e:
        return {"description": f"mwapi error: Conection error - {e}"}, 502

    except Exception as e:
        return {"description": f"mwapi error: {e}, {type(e)}"}, 500

    try:
        wikitext = result['parse']['wikitext']

        num_ref_singleton, num_ref_tag, num_ref_sft = count_references(wikitext)
        num_ref = num_ref_singleton + num_ref_tag + num_ref_sft

        return {
            "project": project,
            "lang": lang,
            "revid": revid,
            "num_ref_singleton": num_ref_singleton,
            "num_ref_tag": num_ref_tag,
            "num_ref_sft": num_ref_sft,
            "wikitext": wikitext,
            "num_ref": num_ref,
            }

    except Exception as e:
        return {"description": f"Error calculating references from wikitext: {e}, {type(e)}"}, 500

def validate_api_args(project, lang):
    """Validate API arguments."""
    error = ''
    if not lang in LANGUAGE_CODES:
        error = f'Language {lang} is not a valid language. '
    if not project in PROJECTS:
        error+= f'Project {project} is not a valid project.'
    return error

def error_number(code):
    try:
        return CODE_NUMBERS[code]
    except KeyError:
        return 404

@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response

def count_references(wikitext):
    # Compile regular expressions

    # Match references using <ref /> tags
    ref_singleton = re.compile(r'<ref(\s[^/>]*)?/>', re.M | re.I)
    # Match regerences using <ref> and </ref> tags
    ref_tag = re.compile(r'<ref(\s[^/>]*)?>[\s\S]*?</ref>', re.M | re.I)
    # Match shortened footnote templates
    shortened_footnote_templates = re.compile("|".join(SFN_TEMPLATES), re.I)

    # remove comments / lowercase for matching namespace prefixes better
    wikitext = re.sub(r'<!--.*?-->', '', wikitext, flags=re.DOTALL).lower()

    num_ref_singleton = len(ref_singleton.findall(wikitext))
    num_ref_tag = len(ref_tag.findall(wikitext))
    num_ref_sft = len(shortened_footnote_templates.findall(wikitext))

    return (num_ref_singleton, num_ref_tag, num_ref_sft)

@app.route('/api/v1/references/html/<project>/<lang>/<int:revid>')
def get_references_from_html(project, lang, revid):
    error = validate_api_args(project, lang)
    if error:
        return {"description": error}, 400

    try:
        # Request the html
        r = requests.get(f'https://{lang}.{project}.org/w/rest.php/v1/revision/{revid}/html',
                         headers={'User-Agent': USER_AGENT})
        if r.status_code != 200:
            return {"description": build_message(r.text)}, r.status_code

    except Exception as e:
        return {"description": f"Error requesting HTML: {e}, {type(e)}"}, 500

    try:
        article = Article(r.text)
        return {
            "project": project,
            "lang": lang,
            "revid": revid,
            "num_ref": len(article.wikistew.get_citations())
            }
    except Exception as e:
        return {"description": f"Error calculating references from HTML: {e}, {type(e)}"}, 500

@app.route('/api/v1/debug/references/html/<project>/<lang>/<int:revid>')
def get_references_debug_from_html(project, lang, revid):
    error = validate_api_args(project, lang)
    if error:
        return {"description": error}, 400

    try:
        # Request the html
        r = requests.get(f'https://{lang}.{project}.org/w/rest.php/v1/revision/{revid}/html',
                         headers={'User-Agent': USER_AGENT})
        if r.status_code != 200:
            return {"description": build_message(r.text)}, r.status_code

    except Exception as e:
        return {"description": f"Error requesting HTML: {e}, {type(e)}"}, 500

    try:
        article = Article(r.text)
        return {
            "project": project,
            "lang": lang,
            "revid": revid,
            "num_ref": len(article.wikistew.get_citations()), "html": r.text
            }
    except Exception as e:
        return {"description": f"Error calculating references from HTML: {e}, {type(e)}"}, 500


def build_message(text):
    ''' Try to build a more elegant message'''
    try:
        json_response = json.loads(text)
        error_key = json_response['errorKey']
        msg = json_response['messageTranslations']['en']
        return f"{error_key} - {msg}"

    except Exception:
        return text

